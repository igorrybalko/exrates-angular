import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {BankexchangeComponent} from './bankexchange/bankexchange.component';
import {BlogItemComponent} from './blog/blog-item/blog-item.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {BlogListComponent} from './blog/blog-list/blog-list.component';
import {ConverterComponent} from './converter/converter.component';
import {NbuComponent} from './nbu/nbu.component';
import { ContactsComponent } from './other/contacts/contacts.component';

// определение маршрутов
const routes: Routes = [
    { path: '', component: BankexchangeComponent},
    { path: 'nbu', component: NbuComponent},
    { path: 'crypto', loadChildren: () => import('./crypto/crypto.module').then(m => m.CryptoModule)},
    { path: 'converter', component: ConverterComponent},
    { path: 'news', component: BlogListComponent},
    { path: 'news/:id', component: BlogItemComponent},
    { path: 'contacts', component: ContactsComponent},
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class AppRoutingModule {}

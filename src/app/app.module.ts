import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {StoreModule} from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {EffectsModule} from '@ngrx/effects';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';

import {AppRoutingModule} from './app-routing.module';
import {BlogModule} from './blog/blog.module';
import {SharedModule} from './shared/shared.module';

import {HttpService} from "./shared/services/http.service";
import {FooterComponent} from "./shared/components/footer/footer.component";
import {HeaderComponent} from "./shared/components/header/header.component";
import {HeaderMenuComponent} from "./shared/components/header/header-menu/header-menu.component";
import { ForexratesComponent } from './shared/components/forexrates/forexrates.component';
import {NbuService} from "./shared/services/nbu.service";
import {MenuService} from "./shared/services/menu.service";
import {MainConfig} from "./shared/config/mainConfig";
import {BankexchangeService} from './bankexchange/bankexchange.service';
import {BankexchangeComponent} from "./bankexchange/bankexchange.component";
import {ConverterComponent} from './converter/converter.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {NbuComponent} from './nbu/nbu.component';
import {nbuReducer} from './redux/nbu.reducer';
import { ContactsComponent } from './other/contacts/contacts.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {NbuEffects} from './redux/nbu.effects';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    HeaderMenuComponent,
    ForexratesComponent,
    BankexchangeComponent,
    ConverterComponent,
    NotFoundComponent,
    NbuComponent,
    ContactsComponent
  ],
  providers: [
    HttpService,
    NbuService,
    MenuService,
    MainConfig,
    BankexchangeService
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BlogModule,
    StoreModule.forRoot({nbu: nbuReducer}),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    EffectsModule.forRoot([NbuEffects]),
    SharedModule,
    BrowserAnimationsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

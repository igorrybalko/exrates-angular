import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import {BankexchangeService} from './bankexchange.service';

@Component({
  selector: 'app-bankexchange',
  templateUrl: './bankexchange.component.html',
})
export class BankexchangeComponent implements OnInit {

    banks: any = [{}];
    filteredBanks: any = [{}];
    date: any = '';
    askBid: object = {};
    @ViewChild('bankexTable', {static: false}) bankexTable: ElementRef;
    searchValue: string;

  bankexShowCssClass = {
    usd: true,
    eur: false,
    rub: false
  };

  bankexSwitchEls: object[] = [
    {title: 'Доллар', id: 'usd'},
    {title: 'Евро', id: 'eur'},
    {title: 'Рубль', id: 'rub'}
  ];

  documentTitle: string = 'Курс валют в банках Украины';

  constructor(private bankexchangeService: BankexchangeService) { }

  ngOnInit(): void{

    this.bankexchangeService.getRates().subscribe((response) => {

      this.banks = response.organizations;
      this.date = response.date;
      this.askBid = this.getAskBid(this.banks);
      this.copyBanks();
      document.title = this.documentTitle;

    });

  }
  changeCurrency(id: string): void {

    switch (id) {
      case 'eur':
        this.bankexShowCssClass.usd = false;
        this.bankexShowCssClass.eur = true;
        this.bankexShowCssClass.rub = false;
        break;
      case 'rub':
        this.bankexShowCssClass.usd = false;
        this.bankexShowCssClass.eur = false;
        this.bankexShowCssClass.rub = true;
        break;
      default:
        this.bankexShowCssClass.usd = true;
        this.bankexShowCssClass.eur = false;
        this.bankexShowCssClass.rub = false;
    }

  }

  copyBanks(): void {
    this.filteredBanks = this.banks.slice();
  }
  search(): void {
    if (!this.searchValue) {this.copyBanks(); } // when nothing has typed
    this.filteredBanks = this.banks.slice().filter(
        item => item.title.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1
    );
  }

  getAskBid(arr: any[]) {

    let eurBid = [],
        rubBid = [],
        usdBid = [],
        eurAsk = [],
        rubAsk = [],
        usdAsk = [];

    for (let i = 0 ; i < arr.length; i++) {
      if (arr[i].orgType === 1) {

        if (arr[i].currencies.EUR) {
            eurBid.push({
                id: arr[i].id,
                bid: +arr[i].currencies.EUR.bid
            });

            eurAsk.push({
                id: arr[i].id,
                ask: +arr[i].currencies.EUR.ask
            });
        }

        if(arr[i].currencies.USD){
            usdBid.push({
                id: arr[i].id,
                bid: +arr[i].currencies.USD.bid
            });

            usdAsk.push({
                id: arr[i].id,
                ask: +arr[i].currencies.USD.ask
            });
        }

        if(arr[i].currencies.RUB){
            rubBid.push({
                id: arr[i].id,
                bid: +arr[i].currencies.RUB.bid
            });

            rubAsk.push({
                id: arr[i].id,
                ask: +arr[i].currencies.RUB.ask
            });
        }
      }
    }

    rubBid.sort(this.arrSortBid);
    eurBid.sort(this.arrSortBid);
    usdBid.sort(this.arrSortBid);
    rubAsk.sort(this.arrSortAsk);
    eurAsk.sort(this.arrSortAsk);
    usdAsk.sort(this.arrSortAsk);

    let askBid = {
        usd: {
            minBid: usdBid.shift(),
            maxBid: usdBid.pop(),
            minAsk: usdAsk.shift(),
            maxAsk: usdAsk.pop(),
        },
        eur: {
            minBid: eurBid.shift(),
            maxBid: eurBid.pop(),
            minAsk: eurAsk.shift(),
            maxAsk: eurAsk.pop()
        },
        rub: {
            minBid: rubBid.shift(),
            maxBid: rubBid.pop(),
            minAsk: rubAsk.shift(),
            maxAsk: rubAsk.pop()
        },
    };

    return askBid;
  }

    arrSortBid(a, b): number {
      if(a.bid > b.bid) return 1;
      if(a.bid < b.bid)  return -1;
      return 0;
    }

    arrSortAsk(a, b): number {
        if(a.ask > b.ask) return 1;
        if(a.ask < b.ask)  return -1;
        return 0;
    }

}

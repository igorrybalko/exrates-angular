import { Component, OnInit } from '@angular/core';

import {BlogService} from '../blog.service';
import {NewsItem} from '../../shared/models/NewsItem';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
})
export class BlogListComponent implements OnInit {

  documentTitle = 'Новости';
  itemsBlog: NewsItem[];

  constructor(
      private blogService: BlogService
  ) { }
  ngOnInit() {
    this.blogService.getItems().subscribe((response: NewsItem[]) => {
      this.itemsBlog = response;
      document.title = this.documentTitle;

    });
  }

}

import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";

import {BlogListComponent} from "./blog-list/blog-list.component";
import {BlogItemComponent} from "./blog-item/blog-item.component";

// определение маршрутов
const routes: Routes =[
    { path: '', component: BlogListComponent},
    { path: ':id', component: BlogItemComponent},
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class BlogRoutingModule {}
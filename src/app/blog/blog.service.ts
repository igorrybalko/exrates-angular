import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {NewsItem} from "../shared/models/NewsItem";
import {MainConfig} from "../shared/config/mainConfig";

@Injectable()
export class BlogService {

  constructor(private http: HttpClient,
              private conf: MainConfig) { }

  getItems(): Observable<NewsItem[]>{
    return this.http.get<NewsItem[]>(this.conf.getUrl('news'));
  }

}

import { Component, OnInit } from '@angular/core';
import {CryptoService} from "./crypto.service";
import {CryptoModel} from "../shared/models/CryptoModel";

@Component({
  selector: 'app-crypto',
  templateUrl: './crypto.component.html'
})
export class CryptoComponent implements OnInit {

  cryptoCurrencies: CryptoModel[];

  filteredCryptoCurrencies: CryptoModel[];

  documentTitle: string = 'Курс криптовалют на сегодня';
  searchValue: string;

  constructor(private cryptoService: CryptoService) { }

  ngOnInit() {

    this.cryptoService.getRates().subscribe((response: CryptoModel[]) => {

      this.cryptoCurrencies = response;
      this.copyCryptoCurrencies();
      document.title = this.documentTitle;

    });

  }

  copyCryptoCurrencies(): void{
    this.filteredCryptoCurrencies = this.cryptoCurrencies.slice();
  }

  search(): void {
    if(!this.searchValue) this.copyCryptoCurrencies(); // when nothing has typed
    this.filteredCryptoCurrencies = this.cryptoCurrencies.slice().filter(
        item => item.name.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1
    );
  }

}

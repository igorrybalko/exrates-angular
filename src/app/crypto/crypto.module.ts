import { NgModule } from '@angular/core';
import { CryptoComponent } from './crypto.component';
import {CryptoService} from './crypto.service';
import {CryptoRoutingModule} from './crypto-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CryptoRoutingModule,
    SharedModule
  ],
  declarations: [CryptoComponent],
  providers: [
      CryptoService
  ]
})
export class CryptoModule { }

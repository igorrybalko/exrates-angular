import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";

import {CryptoModel} from "../shared/models/CryptoModel";
import {MainConfig} from "../shared/config/mainConfig";

@Injectable()
export class CryptoService {

  constructor(private http: HttpClient,
              private conf: MainConfig) { }

  getRates(): Observable<CryptoModel[]>{
    return this.http.get<CryptoModel[]>(this.conf.getUrl('crypto'));
  }

}

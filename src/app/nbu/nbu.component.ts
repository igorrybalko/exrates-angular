import { Component, OnInit } from '@angular/core'
import { Store } from '@ngrx/store'

import {NbuCurrencyModel} from "../shared/models/NbuCurrencyModel"
import {NbuService} from "../shared/services/nbu.service"
import {AppState} from '../redux/app.state'
import {BeginLoadNbuRate} from '../redux/nbu.action';


@Component({
  selector: 'app-nbu',
  templateUrl: './nbu.component.html'
})
export class NbuComponent implements OnInit {

  nbuCurrencies: NbuCurrencyModel[];
  filteredNbuCurrencies: NbuCurrencyModel[];
  searchValue: string;
  documentTitle = 'Курс валют НБУ на сегодня';

  constructor(private nbuService: NbuService, private store: Store<AppState>) { }

  getStoreData() {
      // this.store.select('nbu').subscribe(data => {
      //     if (!data.nbuCurrencies.length) {
      //         this.nbuService.loadRates().then((result: NbuCurrencyModel[]) => {
      //             this.nbuCurrencies = result;
      //             this.copyNbuCurrencies();
      //         });
      //     } else {
      //         this.nbuCurrencies = data.nbuCurrencies;
      //         this.copyNbuCurrencies();
      //     }
      // });

    this.store.dispatch(new BeginLoadNbuRate());

    this.store.select('nbu').subscribe(data => {

          this.nbuCurrencies = data.nbuCurrencies;
          this.copyNbuCurrencies();

    });

  }

  ngOnInit() {

      this.getStoreData();


    // this.nbuService.getRates('/api/nbu').subscribe((response: NbuCurrencyModel[]) => {
    //
    //   this.nbuCurrencies = response;
    //   this.copyNbuCurrencies();
    //
    //
    // });

      document.title = this.documentTitle;
  }

  copyNbuCurrencies(): void {
      this.filteredNbuCurrencies = this.nbuCurrencies.slice();
  }

  search(): void {
    if (!this.searchValue) {this.copyNbuCurrencies(); } // when nothing has typed
    this.filteredNbuCurrencies = this.nbuCurrencies.slice().filter(
        item => item.txt.toLowerCase().indexOf(this.searchValue.toLowerCase()) > -1
    );
  }

}

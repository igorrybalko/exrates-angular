import {NbuCurrencyModel} from '../shared/models/NbuCurrencyModel';

export interface AppState {
    nbuCurrencies: NbuCurrencyModel[];
}

import {Action} from '@ngrx/store';
import {NbuCurrencyModel} from '../shared/models/NbuCurrencyModel';

export namespace NBU_ACTION {
    export const LOAD_NBURATE = 'LOAD_NBURATE';
    export const BEGIN_LOAD_NBURATE = 'BEGIN_LOAD_NBURATE';
}

export class LoadNbuRate implements Action {

    readonly type = NBU_ACTION.LOAD_NBURATE;

    constructor(public payload: NbuCurrencyModel[]) {

    }

}

export class BeginLoadNbuRate implements Action {

    readonly type = NBU_ACTION.BEGIN_LOAD_NBURATE;

}

export type NbuAction = LoadNbuRate | BeginLoadNbuRate;

import { Injectable } from '@angular/core';
import { Actions, Effect, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import {NbuService} from '../shared/services/nbu.service';
import {LoadNbuRate, NBU_ACTION} from './nbu.action';

@Injectable()
export class NbuEffects {
    constructor(private actions$: Actions, private nbuService: NbuService) {
    }

    nbuLoads$ = createEffect(() => this.actions$.pipe(
        ofType(NBU_ACTION.BEGIN_LOAD_NBURATE),
        mergeMap(() => this.nbuService.getRates()
            .pipe(
                map(data => (new LoadNbuRate(data))),
                catchError(() => EMPTY)
            ))
        )
    );
}

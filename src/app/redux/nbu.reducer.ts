import {NbuCurrencyModel} from '../shared/models/NbuCurrencyModel';
import {NBU_ACTION, LoadNbuRate, NbuAction} from './nbu.action';

const initialState = {
    nbuCurrencies: []
};

export function nbuReducer(state = initialState, action: NbuAction) {

    switch (action.type) {
        case NBU_ACTION.LOAD_NBURATE:
            return {
                ...state,
                nbuCurrencies: [...action.payload]
            }
        default:
            return state;
    }

}

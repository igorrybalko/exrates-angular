import { Component, OnInit } from '@angular/core';
import {ForexratesService} from "./forexrates.service";
import {ForexModel} from "../../models/ForexModel";
import {MainConfig} from '../../config/mainConfig';

@Component({
  selector: 'app-forexrates',
  templateUrl: './forexrates.component.html',
  providers: [ForexratesService]
})
export class ForexratesComponent implements OnInit {
  
  date: Date;
  rates: any[] = [];

  constructor(
      private forexRates: ForexratesService,
      private conf: MainConfig
  ) { }

  ngOnInit() {
    this.forexRates.getRates().subscribe( (response: ForexModel) =>{
      
      this.date = new Date(response['timestamp'] * 1000);

      for(let rate in response['quotes']){
        this.rates.push({code: rate, rate: response['quotes'][rate]});
      }

    });
    
    

    
  }

}

import { Component, OnInit } from '@angular/core';
import {MenuService} from "../../../services/menu.service";
import {Menu} from "../../../models/Menu";

declare var topMenu: any;

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html'
})
export class HeaderMenuComponent implements OnInit {

  menuItems: Menu = topMenu;
  showMenu: boolean = false;

  constructor(private menuService: MenuService) { }

  ngOnInit() {

  }

  openMenu(){
    this.showMenu = !this.showMenu;
  }

  closeMenu(){
    this.showMenu = false;
  }

}

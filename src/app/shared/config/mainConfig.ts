import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class MainConfig{

    private apiUrl: string = environment.baseUrl;

    nbuUrl: string = 'nbu';
    forexUrl: string = 'forex';
    banksUrl: string = 'banks';
    newsUrl: string = 'news';
    cryptoUrl: string = 'crypto';

    getUrl(key): string {
       return this.apiUrl + this[key + 'Url'];
    }

    getApiUrl(): string{
        return this.apiUrl;
    }
}
interface ForexCurrencies{
    USD: number,
    GBP: number,
    CHF: number,
    JPY: number,
    CNY: number,
    RUB: number,
    PLN: number
}

export class ForexModel{

    constructor(
        public success: boolean,
        public timestamp: number,
        public base: string,
        public date: string,
        public rates: ForexCurrencies
    ){}
}
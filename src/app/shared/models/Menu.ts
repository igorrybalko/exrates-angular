export class Menu{

    constructor(
        public title: string,
        public url: string,
        public id: number
    ){}
}
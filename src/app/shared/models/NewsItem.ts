export class NewsItem{
    
    constructor(
        public title: string,
        public description: string,
        public fulltext: string,
        public date: string,
        public img: string,
        public id: number
    ){}
}
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) { }

  get(url: string){
    return this.http.get(url);
  }

  getXML(url: string){

  	// const headers = new HttpHeaders({ 'Accept': 'application/xml', responseType:'text' });
	// const options = { headers: headers };

	return this.http.get(url, {responseType: 'text'});
  }

}

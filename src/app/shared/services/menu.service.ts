import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";

import {Menu} from "../models/Menu";
import {MainConfig} from "../config/mainConfig";


@Injectable()
export class MenuService {

    constructor(private http: HttpClient,
    private conf: MainConfig) { }

    getMenu(id: number): Observable<Menu>{
        let apiUrl = this.conf.getApiUrl();
        return this.http.get<Menu>(apiUrl + '/api/menu/items/' + id);
    }

}

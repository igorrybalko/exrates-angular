import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {NbuCurrencyModel} from '../models/NbuCurrencyModel';
import {MainConfig} from '../config/mainConfig';

@Injectable()
export class NbuService {

  constructor(private http: HttpClient,
              private conf: MainConfig,
              ) { }

  getRates(): Observable<NbuCurrencyModel[]> {
    return this.http.get<NbuCurrencyModel[]>(this.conf.getUrl('nbu'));
  }

  // loadRates(): Observable<NbuCurrencyModel[]> {
  //
  //
  //
  //         this.http.get(this.conf.getUrl('nbu')).subscribe((data: NbuCurrencyModel[]) => {
  //
  //         }, error => {
  //             console.log(error);
  //
  //         });
  //
  //
  //
  // }

}

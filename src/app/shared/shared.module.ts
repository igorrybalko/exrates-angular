import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InputTextModule} from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import {SpinnerModule} from 'primeng/spinner';
import {ButtonModule} from 'primeng/button';
import {TabViewModule} from 'primeng/tabview';

@NgModule({
    imports: [],
    declarations: [],
    exports: [
        CommonModule,
        InputTextModule,
        FormsModule,
        DropdownModule,
        SpinnerModule,
        ButtonModule,
        TabViewModule
    ]
})
export class SharedModule {}
